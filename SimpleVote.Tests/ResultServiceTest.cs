using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using SimpleVote.Models;
using SimpleVote.Services;
using Xunit;

namespace SimpleVote.Tests
{
    public class ResultServiceTest
    {
        private readonly ResultIrService _resultService;
        private readonly List<ResponseRanked> _question1Answers;
        private readonly List<ResponseRanked> _question2Answers;
        private readonly List<ResponseRanked> _question2AnswersReAssigned;
        
        public ResultServiceTest()
        {
            var config = new MapperConfiguration(cfg => { });

            _resultService = new ResultIrService(new Mapper(config));
            
            _question1Answers = new List<ResponseRanked>()
            {
                new ResponseRanked()
                {
                    Question = new Question(1),
                    Answer = new Answer(1),
                    Id = 1,
                    Rank = 1
                },
                new ResponseRanked()
                {
                    Question = new Question(1),
                    Answer = new Answer(2),
                    Id = 2,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    Question = new Question(1),
                    Answer = new Answer(3),
                    Id = 3,
                    Rank = 3
                },
                new ResponseRanked()
                {
                    Question = new Question(1),
                    Answer = new Answer(1),
                    Id = 4,
                    Rank = 1
                },
                new ResponseRanked()
                {
                    Question = new Question(1),
                    Answer = new Answer(2),
                    Id = 5,
                    Rank = 3
                },
                new ResponseRanked()
                {
                    Question = new Question(1),
                    Answer = new Answer(3),
                    Id = 6,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    Question = new Question(1),
                    Answer = new Answer(1),
                    Id = 7,
                    Rank = 3
                },
                new ResponseRanked()
                {
                    Question = new Question(1),
                    Answer = new Answer(2),
                    Id = 8,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    Question = new Question(1),
                    Answer = new Answer(3),
                    Id = 9,
                    Rank = 1
                }
            };
            
            _question2Answers = new List<ResponseRanked>()
            {
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(4),
                    Id = 11,
                    Rank = 1
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(5),
                    Id = 12,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(6),
                    Id = 13,
                    Rank = 3
                },
                
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(4),
                    Id = 14,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(5),
                    Id = 15,
                    Rank = 1
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(6),
                    Id = 16,
                    Rank = 3
                },
                
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(4),
                    Id = 17,
                    Rank = 3
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(5),
                    Id = 18,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(6),
                    Id = 19,
                    Rank = 1
                }
            };
            
            _question2AnswersReAssigned = new List<ResponseRanked>()
            {
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(4),
                    Id = 11,
                    Rank = 1
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(5),
                    Id = 12,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(6),
                    Id = 13,
                    Rank = 3
                },
                
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(4),
                    Id = 14,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(5),
                    Id = 15,
                    Rank = 1
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(6),
                    Id = 16,
                    Rank = 3
                },
                
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(4),
                    Id = 17,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(5),
                    Id = 18,
                    Rank = 1
                },
                new ResponseRanked()
                {
                    Question = new Question(2),
                    Answer = new Answer(6),
                    Id = 19,
                    Rank = 0
                }
            };
            
            
        }

        [Fact]
        public void InstantRunoff1()
        {
            /*
            var data = new Dictionary<string, List<Response>>()
            {
                {
                    "user1", new List<Response>()
                    {
                        new ResponseIr()
                        {
                            Question = new Question(1),
                            Answer = new Answer(1),
                            Id = 1,
                            Rank = 1
                        },
                        new ResponseIr()
                        {
                            Question = new Question(1),
                            Answer = new Answer(2),
                            Id = 2,
                            Rank = 2
                        },
                        new ResponseIr()
                        {
                            Question = new Question(1),
                            Answer = new Answer(3),
                            Id = 3,
                            Rank = 3
                        },
                    }
                },
                {
                    "user2", new List<Response>()
                    {
                        new ResponseIr()
                        {
                            Question = new Question(1),
                            Answer = new Answer(1),
                            Id = 4,
                            Rank = 1
                        },
                        new ResponseIr()
                        {
                            Question = new Question(1),
                            Answer = new Answer(2),
                            Id = 5,
                            Rank = 2
                        },
                        new ResponseIr()
                        {
                            Question = new Question(1),
                            Answer = new Answer(3),
                            Id = 6,
                            Rank = 3
                        },
                    }
                },
                {
                    "user3", new List<Response>()
                    {
                        new ResponseIr()
                        {
                            Question = new Question(1),
                            Answer = new Answer(1),
                            Id = 7,
                            Rank = 3
                        },
                        new ResponseIr()
                        {
                            Question = new Question(1),
                            Answer = new Answer(2),
                            Id = 8,
                            Rank = 2
                        },
                        new ResponseIr()
                        {
                            Question = new Question(1),
                            Answer = new Answer(3),
                            Id = 9,
                            Rank = 1
                        },
                    }
                }
            };

            var result = _resultService.InstantRunoffRemoveAndReduce(data);

            Assert.Equal(data.SelectMany(d => d.Value).ToList().Count, 9);
            Assert.Equal(result.SelectMany(d => d.Value).ToList().Count, 6);
            */
        }

        [Fact]
        public void GetAnswerCounts()
        {
            var result = _resultService.GetAnswerCounts(_question1Answers);

            Assert.Equal(result.Count, 3);


            Assert.Equal(result[0].AnswerId, 1); // Answer 1
            Assert.Equal(result[0].OneCount, 2); // Answer 1
            Assert.Equal(result[0].TwoCount, 0); // Answer 1
            Assert.Equal(result[0].ThreeCount, 1); // Answer 1

            Assert.Equal(result[1].AnswerId, 3); // Answer 3
            Assert.Equal(result[1].OneCount, 1); // Answer 3
            Assert.Equal(result[1].TwoCount, 1); // Answer 3
            Assert.Equal(result[1].ThreeCount, 1); // Answer 3

            Assert.Equal(result[2].AnswerId, 2); // Answer 2
            Assert.Equal(result[2].OneCount, 0); // Answer 2
            Assert.Equal(result[2].TwoCount, 2); // Answer 2
            Assert.Equal(result[2].ThreeCount, 1); // Answer 2
        }

        [Fact]
        public void GetLosingAnswers_ForQuestion1()
        {

            var result = _resultService.GetLosingAnswer(_question1Answers, new List<int>());

            Assert.Equal(result, 2);
        }
        
        [Fact]
        public void GetLosingAnswers_ForQuestion2_TiedFirstPlaces_Different2ndPlaces()
        {

            var result = _resultService.GetLosingAnswer(_question2Answers, new List<int>());

            Assert.Equal(result, 6);
        }
        
        [Fact]
        public void GetLosingAnswers_ForQuestion2_ReAssigned()
        {

            var result = _resultService.GetLosingAnswer(_question2AnswersReAssigned, new List<int>(){6});

            Assert.Equal(result, 4);
        }

        [Fact]
        public void ReAssignCounts()
        {
            var data = new List<ResponseRanked>()
            {
                new ResponseRanked()
                {
                    SubmittedBy = "1",
                    Question = new Question(1),
                    Answer = new Answer(1),
                    Id = 1,
                    Rank = 1
                },
                new ResponseRanked()
                {
                    SubmittedBy = "1",
                    Question = new Question(1),
                    Answer = new Answer(2),
                    Id = 2,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    SubmittedBy = "1",
                    Question = new Question(1),
                    Answer = new Answer(3),
                    Id = 3,
                    Rank = 3
                },
                new ResponseRanked()
                {
                    SubmittedBy = "2",
                    Question = new Question(1),
                    Answer = new Answer(1),
                    Id = 4,
                    Rank = 1
                },
                new ResponseRanked()
                {
                    SubmittedBy = "2",
                    Question = new Question(1),
                    Answer = new Answer(2),
                    Id = 5,
                    Rank = 3
                },
                new ResponseRanked()
                {
                    SubmittedBy = "2",
                    Question = new Question(1),
                    Answer = new Answer(3),
                    Id = 6,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    SubmittedBy = "3",
                    Question = new Question(1),
                    Answer = new Answer(1),
                    Id = 7,
                    Rank = 3
                },
                new ResponseRanked()
                {
                    SubmittedBy = "3",
                    Question = new Question(1),
                    Answer = new Answer(2),
                    Id = 8,
                    Rank = 2
                },
                new ResponseRanked()
                {
                    SubmittedBy = "3",
                    Question = new Question(1),
                    Answer = new Answer(3),
                    Id = 9,
                    Rank = 1
                }
            };
            
            Assert.Equal(data.Count(r => r.Rank.Equals(0)), 0);
            Assert.Equal(data.Count(r => r.Rank.Equals(1)), 3);
            Assert.Equal(data.Count(r => r.Rank.Equals(2)), 3);
            Assert.Equal(data.Count(r => r.Rank.Equals(3)), 3);

            var results = _resultService.ReAssignRanks(data, 3);
            
            Assert.Equal(results.Count(r => r.Rank.Equals(0)), 1);
            Assert.Equal(results.Count(r => r.Rank.Equals(1)), 3);
            Assert.Equal(results.Count(r => r.Rank.Equals(2)), 3);
            Assert.Equal(results.Count(r => r.Rank.Equals(3)), 2);
        }
    }
}