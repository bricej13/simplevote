import axios from '~/plugins/axios'
import QuestionIR from '~/components/Questions/IR'
import Question from './Question'

class IRQuestion extends Question {
  constructor (question) {
    super(question)
    this.view = QuestionIR
    this.submit = function () {
      return axios.post('/api/responses',
        this.ballot.questions.map(q => {
          return {
            ballotId: this.ballot.id,
            questionId: q.id,
            ranks: q.answers.reduce(function (sum, a) {
              sum[a.id] = a.rank
              return sum
            }, {}),
            sessionId: localStorage.getItem('sessionId'),
            voteType: this.ballot.voteType
          }
        })
      )
    }
  }
}

export default IRQuestion
