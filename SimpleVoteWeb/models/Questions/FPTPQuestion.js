import axios from '~/plugins/axios'
import QuestionFptp from '~/components/Questions/FPTP'
import Question from './Question'

class FPTPQuestion extends Question {
  constructor (question) {
    super(question)
    this.view = QuestionFptp
    this.submit = function () {
      return axios.post('/api/responses',
        this.ballot.questions.map(q => {
          return {
            ballotId: this.ballot.id,
            questionId: q.id,
            answerId: find(q.answers, q => q.selected).id,
            sessionId: localStorage.getItem('sessionId'),
            voteType: this.ballot.voteType
          }
        })
      )
    }
  }
}

export default FPTPQuestion
