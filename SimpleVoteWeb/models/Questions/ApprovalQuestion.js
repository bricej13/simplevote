import filter from 'lodash/filter'
import axios from '~/plugins/axios'
import QuestionApproval from '~/components/Questions/Approval'
import Question from './Question'

class ApprovalQuestion extends Question {
  constructor (question) {
    super(question)
    this.view = QuestionApproval
    this.submit = function () {
      return axios.post('/api/responses',
        this.ballot.questions.map(q => {
          return {
            ballotId: this.ballot.id,
            questionId: q.id,
            answers: filter(q.answers, q => q.selected).map(g => g.id),
            sessionId: localStorage.getItem('sessionId'),
            voteType: this.ballot.voteType
          }
        })
      )
    }
  }
}

export default ApprovalQuestion
