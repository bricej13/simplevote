import assignIn from 'lodash/assignIn'

class Question {
  constructor (question) {
    this.description = question.description
    this.id = question.id
    this.valid = false
    this.answers = assignIn(question.answers, {
      selected: false,
      rank: null
    })
  }
}

export default Question
