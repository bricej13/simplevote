import Vuex from 'vuex'

const createStore = () => {
  return new Vuex.Store({
    state: {
      voteTypes: {}
    },
    mutations: {
      setVoteTypes (state, data) {
        state.voteTypes = data
      }
    },
    getters: {
      getVoteTypes (state) {
        return state.voteTypes
      }
    }
  })
}

export default createStore
