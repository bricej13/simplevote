import Fingerprint2 from 'fingerprintjs2'
export default function (context) {
  if (process.browser) {
    let fingerprint = window.localStorage.getItem('sessionId')
    if (!fingerprint) {
      new Fingerprint2().get(function (result, components) {
        localStorage.setItem('sessionId', result)
      })
    }

    // context.sessionId = fingerprint
  }
}

/*
function getGuid () { // GUID generator
  return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
    (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
  )
}
*/
