module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Simple Vote',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Simple Vote web front-end' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  mode: 'spa',
  transition: {
    name: 'page',
    mode: 'out-in'
  },
  css: [
    'assets/main.css',
    'iview/dist/styles/iview.css'
  ],
  env: {
    baseUrl: process.env.SIMPLEVOTE_BASE_URL || 'http://localhost:5000'
  },
  router: {
    mode: process.env.SIMPLEVOTE_ROUTER_MODE || 'history'
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#0000EE' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    babel: {
      plugins: [['import', {
        libraryName: 'iview',
        libraryDirectory: 'src/components'
      }]]
    },
    extend (config, ctx) {
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
      config.output.publicPath = '_nuxt/'
    }
  }
}
