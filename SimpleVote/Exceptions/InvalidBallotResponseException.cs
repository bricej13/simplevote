﻿using System;
using System.Runtime.Serialization;

namespace SimpleVote.Exceptions
{
    public class InvalidBallotResponseException : Exception
    {
        public InvalidBallotResponseException()
        {
        }

        public InvalidBallotResponseException(string message) : base(message)
        {
        }

        public InvalidBallotResponseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidBallotResponseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}