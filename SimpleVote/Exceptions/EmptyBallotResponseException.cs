﻿using System;
using System.Runtime.Serialization;

namespace SimpleVote.Exceptions
{
    public class EmptyBallotResponseException : Exception
    {
        public EmptyBallotResponseException()
        {
        }

        public EmptyBallotResponseException(string message) : base(message)
        {
        }

        public EmptyBallotResponseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected EmptyBallotResponseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}