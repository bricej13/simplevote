﻿using System;
using System.Runtime.Serialization;

namespace SimpleVote.Exceptions
{
    public class DuplicateUserResponseException : Exception
    {
        public DuplicateUserResponseException()
        {
        }

        public DuplicateUserResponseException(string message) : base(message)
        {
        }

        public DuplicateUserResponseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected DuplicateUserResponseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}