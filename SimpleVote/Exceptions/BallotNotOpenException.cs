﻿using System;
using System.Runtime.Serialization;

namespace SimpleVote.Exceptions
{
    public class BallotNotOpenException : Exception
    {
        public BallotNotOpenException()
        {
        }

        public BallotNotOpenException(string message) : base(message)
        {
        }

        public BallotNotOpenException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected BallotNotOpenException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}