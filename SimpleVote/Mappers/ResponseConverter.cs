﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Enums;

namespace SimpleVote.Mappers
{
    public class ResponseConverter : JsonConverter
    {
        public override bool CanWrite => false;
        public override bool CanRead => true;

        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(ResponseDto);
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            throw new InvalidOperationException("Use default serialization.");
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);
            var response = default(ResponseDto);
            switch (jsonObject["voteType"].ToObject<VoteTypeEnum>())
            {
                case VoteTypeEnum.FirstPastThePost:
                    response = new ResponseFptpDto();
                    break;
                case VoteTypeEnum.InstantRunoff:
                    response = new ResponseRankedDto();
                    break;
                case VoteTypeEnum.Approval:
                    response = new ResponseApprovalDto();
                    break;
                case VoteTypeEnum.Condocert:
                    response = new ResponseRankedDto();
                    break;
            }
            serializer.Populate(jsonObject.CreateReader(), response);
            return response;
        }
    }
}