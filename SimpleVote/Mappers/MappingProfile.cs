﻿using AutoMapper;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;

namespace SimpleVote.Mappers
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<BallotCreateDto, Ballot>();
            CreateMap<Ballot, BallotDto>().ReverseMap();
            CreateMap<Question, QuestionDto>().ReverseMap();
            CreateMap<QuestionCreateDto, Question>();
            CreateMap<Answer, AnswerDto>().ReverseMap();
            CreateMap<AnswerCreateDto, Answer>();
        }
    }
}