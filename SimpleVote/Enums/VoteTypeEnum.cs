﻿namespace SimpleVote.Enums
{
    public enum VoteTypeEnum
    {
        FirstPastThePost,
        InstantRunoff,
        Approval,
        Condocert
    }
}