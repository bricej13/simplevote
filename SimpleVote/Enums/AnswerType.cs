﻿namespace SimpleVote.Enums
{
    public enum AnswerTypeEnum
    {
        Select,
        Rank,
        Pairwise
    }
}