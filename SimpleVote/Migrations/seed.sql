DELETE FROM VoteType;
INSERT OR REPLACE INTO VoteType (Id, AnswerType, CreatedDate, Description, Name, UpdatedDate)
VALUES (
  0,
  0,
  CURRENT_TIMESTAMP,
  'Voters indicate a single answer of their choice, and the answer that receives the most votes wins: also known as winner takes all.',
  'First Past the Post',
  CURRENT_TIMESTAMP
);
INSERT OR REPLACE INTO VoteType (Id, AnswerType, CreatedDate, Description, Name, UpdatedDate)
VALUES (
  1,
  1,
  CURRENT_TIMESTAMP,
  'In Instant Runoff voting, voters can rank the answers in order of preference. Ballots are initially counted for each voter''s top choice. If an answer secures more than half of these votes, that answer wins. Otherwise, the answer in last place is eliminated and removed from consideration. The top remaining choices on all the ballots are then counted again. This process repeats until one answer is the top remaining choice of a majority of the voters.',
  'Instant Runoff',
  CURRENT_TIMESTAMP
);
INSERT OR REPLACE INTO VoteType (Id, AnswerType, CreatedDate, Description, Name, UpdatedDate)
VALUES (
  2,
  0,
  CURRENT_TIMESTAMP,
  'Approval voting is a single-winner system. Each voter may select (i.e. approve) any number of answers. The winner is the most-approved answer.',
  'Approval',
  CURRENT_TIMESTAMP
);
INSERT OR REPLACE INTO VoteType (Id, AnswerType, CreatedDate, Description, Name, UpdatedDate)
VALUES (
  3,
  2,
  CURRENT_TIMESTAMP,
  'Elects the candidate that would win a majority of the vote in all of the head-to-head elections against each of the other candidates, whenever there is such a candidate.',
  'Condecert',
  CURRENT_TIMESTAMP);

DROP VIEW IF EXISTS CondocertResult;
CREATE VIEW CondocertResult AS
  WITH TempTable (BallotId, QuestionId, Answer1Id, Answer2Id, Count) AS (
    -- Condocert Results Query
      SELECT
        BallotId,
        QuestionId,
        Answer1Id,
        Answer2Id,
        SUM(IsGreater)
      FROM
        (
          SELECT
            r1.AnswerId       Answer1Id,
            r2.AnswerId       Answer2Id,
            r1.Rank < r2.Rank IsGreater,
            r1.BallotId,
            r1.QuestionId
          FROM Response r1
            CROSS JOIN Response r2
          WHERE r1.BallotId = r2.BallotId
                AND r1.AnswerId != r2.AnswerId
                AND r1.Rank IS NOT null
                AND r2.Rank IS NOT null
        )
      GROUP BY Answer1Id, Answer2Id, BallotId, QuestionId
  )
  SELECT
    t1.BallotId,
    t1.QuestionId,
    t1.Answer1Id,
    T1.Answer2Id,
    t1.Count,
    t1.Count - T2.Count AS Margin
  FROM TempTable t1
    JOIN TempTable T2 ON T2.Answer1Id = t1.Answer2Id AND T2.Answer2Id = t1.Answer1Id
  ORDER BY Margin
    DESC;
