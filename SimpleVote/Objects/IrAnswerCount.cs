﻿using System;
using SimpleVote.Models;

namespace SimpleVote.Objects
{
    public class IrAnswerCount : IComparable
    {
        public int QuestionId { get; set; }
        public int AnswerId { get; set; }
        public int OneCount { get; set; }
        public int TwoCount { get; set; }
        public int ThreeCount { get; set; }

        public IrAnswerCount()
        {
        }


        public int CompareTo(object obj)
        {
            throw new NotImplementedException();
        }
    }
}