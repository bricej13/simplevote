﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleVote.Models
{
    public class CondocertResult
    {
        public int BallotId { get; set; }
        public int QuestionId { get; set; }
        public int Answer1Id { get; set; }
        public int Answer2Id { get; set; }
        public int Count { get; set; }
        public int Margin { get; set; }
        
        public virtual Ballot Ballot { get; set; }
        public virtual Answer Answer1 { get; set; }
        public virtual Answer Answer2 { get; set; }
        
    }
}