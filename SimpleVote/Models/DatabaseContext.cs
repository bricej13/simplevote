﻿using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace SimpleVote.Models
{
    public class DatabaseContext : DbContext
    {
        public DbSet<Ballot> Ballots { get; set; }
        public DbSet<Question> Question { get; set; }
        public DbSet<Answer> Answer { get; set; }
        public DbSet<Response> Response { get; set; }
        public DbSet<VoteType> VoteType { get; set; }
        public DbSet<CondocertResult> CondocertResult { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder {DataSource = "db.sqlite"};
            var connectionString = connectionStringBuilder.ToString();
            var connection = new SqliteConnection(connectionString);

            optionsBuilder.UseSqlite(connection);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Compound Keys
            modelBuilder.Entity<CondocertResult>()
                .HasKey(c => new {c.BallotId, c.Answer1Id});

            // Inheritance
            modelBuilder.Entity<ResponseSelect>().HasBaseType<Response>();
            modelBuilder.Entity<ResponseRanked>().HasBaseType<Response>();

            // Auto-populate
            modelBuilder.Entity<Ballot>()
                .Property(b => b.CreatedDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Question>()
                .Property(b => b.CreatedDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Answer>()
                .Property(b => b.CreatedDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP");
            modelBuilder.Entity<Response>()
                .Property(b => b.CreatedDate)
                .HasDefaultValueSql("CURRENT_TIMESTAMP");

            // Indexes
            // modelBuilder.Entity<ResponseFptp>()
            // .HasIndex(r => new {r.Ballot, r.SubmittedBy})
            // .IsUnique();
            
        }
    }
}