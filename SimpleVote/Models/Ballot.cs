﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SimpleVote.Enums;

namespace SimpleVote.Models
{
    public class Ballot
    {
        [Key]
        public int Id { get; set; }

        public string UniqueId { get; set; }
        public string EditId { get; set; }
        public string Name { get; set; }
        public bool SingleQuestion { get; set; }
        public Enums.VoteTypeEnum? VoteType { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public bool IsOpen { get; set; }
        public bool IsClosed { get; set; }
        public DateTime CreatedDate { get; set; }
        
        [InverseProperty("Ballot")]
        public virtual ICollection<Question> Questions { get; set; }
        
        [InverseProperty("Ballot")]
        public virtual ICollection<Response> Responses { get; set; }

        public Ballot()
        {
            this.Questions = new List<Question>();
        }

        public Ballot(int id)
        {
            Id = id;
        }
    }
}
