﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SimpleVote.Enums;

namespace SimpleVote.Models
{
    public class ResponseRanked : Response
    {
        public int Rank { get; set; }
    }
}