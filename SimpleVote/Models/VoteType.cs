﻿using System;
using System.ComponentModel.DataAnnotations;
using SimpleVote.Enums;

namespace SimpleVote.Models
{
    public class VoteType
    {
        [Key]
        public Enums.VoteTypeEnum Id { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }
        public AnswerTypeEnum AnswerType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public VoteType()
        {
        }

        public override bool Equals(object obj)
        {
            if (obj is VoteType that)
                return this.Id.Equals(that.Id);

            return false;
        }
    }
}