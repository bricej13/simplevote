﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SimpleVote.Enums;

namespace SimpleVote.Models
{
    public class Question
    {
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public virtual Ballot Ballot { get; set; }
        
        [InverseProperty("Question")]
        public virtual ICollection<Answer> Answers { get; set; }
        
        [InverseProperty("Question")]
        public virtual ICollection<Response> Responses { get; set; }

        public Question()
        {
            this.Answers = new List<Answer>();
        }

        public Question(int id)
        {
            Id = id;
        }
    }
}