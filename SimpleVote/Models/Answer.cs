﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SimpleVote.Enums;

namespace SimpleVote.Models
{
    public class Answer
    {
        [Key]
        public int Id { get; set; }

        public string Description { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }

        public virtual Question Question { get; set; }

        public Answer()
        {
        }

        public Answer(int id)
        {
            Id = id;
        }

        public override bool Equals(object obj)
        {
            if (obj is Answer that)
                return this.Id.Equals(that.Id);
            
            return false;
        }
    }
}