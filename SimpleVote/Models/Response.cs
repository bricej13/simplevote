﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SimpleVote.Enums;

namespace SimpleVote.Models
{
    public class Response
    {
        [Key]
        public int Id { get; set; }

        public string SubmittedBy { get; set; }
        public Ballot Ballot { get; set; }
        public Question Question { get; set; }
        public Answer Answer { get; set; }
        
        public DateTime CreatedDate { get; set; }
    }
}