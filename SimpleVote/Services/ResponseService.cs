﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Enums;
using SimpleVote.Exceptions;
using SimpleVote.Models;

namespace SimpleVote.Services
{
    public class ResponseService : IResponseService
    {
        private readonly DatabaseContext _db = new DatabaseContext();

        public int Save(Collection<ResponseDto> dto)
        {
            Validate(dto);

            // Save responses
            foreach (ResponseDto responseDto in dto)
            {
                switch (responseDto.VoteType)
                {
                    case VoteTypeEnum.FirstPastThePost:
                        if (responseDto is ResponseFptpDto fptpDto)
                        {
                            SaveFptpResponse(fptpDto);
                        }
                        break;
                    case VoteTypeEnum.InstantRunoff:
                        if (responseDto is ResponseRankedDto irDto)
                        {
                            SaveRankedResponse(irDto);
                        }
                        break;
                    case VoteTypeEnum.Approval:
                        if (responseDto is ResponseApprovalDto approvalDto)
                        {
                            SaveApprovalResponse(approvalDto);
                        }
                        break;

                    case VoteTypeEnum.Condocert:
                        if (responseDto is ResponseRankedDto condocertDto)
                        {
                            SaveRankedResponse(condocertDto);
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            return _db.SaveChanges();
        }
        private void SaveFptpResponse(ResponseFptpDto fptpDto)
        {
            _db.Response.Add(new ResponseSelect()
            {
                Ballot = _db.Ballots.FirstOrDefault(b => b.Id.Equals(fptpDto.BallotId)),
                Answer = _db.Answer.FirstOrDefault(q => q.Id.Equals(fptpDto.AnswerId)),
                Question = _db.Question.FirstOrDefault(q => q.Id.Equals(fptpDto.QuestionId)),
                SubmittedBy = fptpDto.SessionId
            });
        }

        private void SaveRankedResponse(ResponseRankedDto dto)
        {
            var tmpBallot = _db.Ballots.FirstOrDefault(b => b.Id.Equals(dto.BallotId));
            var tmpQuestion = _db.Question.FirstOrDefault(q => q.Id.Equals(dto.QuestionId));
            foreach (var keyValuePair in dto.Ranks)
            {
                _db.Response.Add(new ResponseRanked()
                {
                    Ballot = tmpBallot,
                    Question = tmpQuestion,
                    SubmittedBy = dto?.SessionId,
                    Rank = keyValuePair.Value,
                    Answer = _db.Answer.FirstOrDefault(q => q.Id.Equals(keyValuePair.Key)),
                });
            }
        }

        private void SaveApprovalResponse(ResponseApprovalDto approvalDto)
        {
            var tmpBallot = _db.Ballots.FirstOrDefault(b => b.Id.Equals(approvalDto.BallotId));
            var tmpQuestion = _db.Question.FirstOrDefault(q => q.Id.Equals(approvalDto.QuestionId));
            var tmpAnswers = _db.Answer.Where(a => approvalDto.Answers.Contains(a.Id)).ToList();

            if (tmpAnswers.Count != approvalDto.Answers.Count)
            {
                throw new InvalidBallotResponseException("Not all answer ids were valid answers");
            }

            approvalDto.Answers.ForEach(answerId =>
            {
                _db.Response.Add(new ResponseSelect()
                {
                    Answer = tmpAnswers.FirstOrDefault(q => q.Id.Equals(answerId)),
                    Ballot = tmpBallot,
                    Question = tmpQuestion,
                    SubmittedBy = approvalDto.SessionId
                });
            });
        }

        private void Validate(Collection<ResponseDto> dto)
        {
            // Validation
            if (dto.Count.Equals(0))
            {
                throw new EmptyBallotResponseException();
            }

            var ballot = _db.Ballots.Find(dto[0].BallotId);

            if (!ballot.IsOpen || ballot.IsClosed)
            {
                throw new BallotNotOpenException();
            }
            if (_db.Response.Any(r => r.Ballot.Id == dto[0].BallotId && r.SubmittedBy == dto[0].SessionId))
            {
                throw new DuplicateUserResponseException();
            }
        }

    }
}