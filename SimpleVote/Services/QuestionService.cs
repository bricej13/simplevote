﻿using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;

namespace SimpleVote.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly DatabaseContext _db = new DatabaseContext();
        private readonly IMapper _mapper;

        public QuestionService(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Question Create(QuestionCreateDto dto)
        {
            var ballot = _db.Ballots.Find(dto.BallotId);
            ballot.Questions.Add(_mapper.Map<QuestionCreateDto, Question>(dto));

            _db.SaveChanges();

            return ballot.Questions.FirstOrDefault();
        }

        public void Delete(int id)
        {
            var question = _db.Question.Include(q => q.Answers).First(q => q.Id == id);
            
            // Remove Answers
            question.Answers.ToList().ForEach(a => _db.Answer.Remove(a));
            
            // Remove Question
            _db.Question.Remove(question);
            
            _db.SaveChanges();
        }
    }
}