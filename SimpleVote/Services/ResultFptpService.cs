﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Enums;
using SimpleVote.Models;
using SimpleVote.Objects;

namespace SimpleVote.Services
{
    public class ResultFptpService : IResultService
    {
        private readonly DatabaseContext _db = new DatabaseContext();
        private readonly IMapper _mapper;

        public ResultFptpService(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IList<IResultDto>  GetResults(string uniqueId)
        {
            var results = _db.Response.Where(r => r.Ballot.UniqueId == uniqueId || r.Ballot.EditId == uniqueId)
                .Include(r => r.Answer)
                .Include(r => r.Question)
                .GroupBy(r => new {r.Answer, r.Question})
                .Select(group => new ResultFptpDto()
                {
                    Answer = _mapper.Map<Answer, AnswerDto>(group.Key.Answer),
                    Question = _mapper.Map<Question, QuestionDto>(group.Key.Question),
                    Count = group.Count()
                })
                .Cast<IResultDto>()
                .ToList();

            return results;
        }
    }
}