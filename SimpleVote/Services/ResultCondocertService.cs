﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;

namespace SimpleVote.Services
{
    public class ResultCondocertService : IResultService
    {
        private readonly DatabaseContext _db = new DatabaseContext();
        private readonly IMapper _mapper;

        public ResultCondocertService(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IList<IResultDto> GetResults(string uniqueId)
        {
            var questions = _db.Question
                .Where(q => q.Ballot.UniqueId == uniqueId || q.Ballot.EditId == uniqueId)
                .Include(q => q.Answers)
                .ToListAsync();
            var results = _db.CondocertResult.Where(r => r.Ballot.UniqueId == uniqueId || r.Ballot.EditId == uniqueId)
                .ToList();
            
            /*
            var originalResults = results.Select(r => new CondocertResult()
            {
                BallotId = r.BallotId,
                QuestionId = r.QuestionId,
                Answer1Id = r.Answer1Id,
                Answer2Id = r.Answer2Id,
                Count = r.Count,
                Margin = r.Margin
            }).ToList();
            
            var winner = HasWinner(results);
            while (!winner.HasValue)
            {
                results = ResolveAmbiguity(results);
                winner = HasWinner(results);
            }
            */

            var dtos = questions.Result.Select(q => new ResultCondocertDto()
            {
                Question = _mapper.Map<Question, QuestionDto>(q),
                Matrix = q.Answers.Aggregate(
                    new Dictionary<AnswerDto, List<CondocertResult>>(),
                    (acc, a) =>
                    {
                        acc.Add(_mapper.Map<Answer, AnswerDto>(a),
                            results.Where(r => r.Answer1Id.Equals(a.Id)).ToList());
                        return acc;
                    })
            }).Cast<IResultDto>().ToList();

            return dtos;
        }

        private List<Tuple<int, int, double>> GetPairwiseTuples(string uniqueId)
        {
            var tuples = _db.Response
                .Where(r1 => r1.Ballot.EditId == uniqueId || r1.Ballot.UniqueId == uniqueId)
                .Cast<ResponseRanked>()
                .SelectMany(r1 => _db.Response
                        .Where(r2 => r2.Ballot.EditId == uniqueId || r2.Ballot.UniqueId == uniqueId)
                        .Cast<ResponseRanked>(),
                    (r1, r2) =>
                        new
                        {
                            AnswerId1 = r1.Answer.Id,
                            AnswerId2 = r2.Answer.Id,
                            IsGreater = r1.Rank < r2.Rank,

                            r1.Ballot.EditId,
                            r1.Ballot.UniqueId,
                        }
                ).Where(r => r.AnswerId1 != r.AnswerId2
                             && (r.EditId == uniqueId || r.UniqueId == uniqueId)
                ).GroupBy(r => new {r.AnswerId1, r.AnswerId2})
                .OrderBy(g => g.Key.AnswerId1).ThenBy(g => g.Key.AnswerId2)
                .Select(g => new Tuple<int, int, double>
                (
                    g.Key.AnswerId1,
                    g.Key.AnswerId2,
                    Math.Sqrt(g.Sum(i => i.IsGreater ? 1 : 0))
                )).ToList();

            return tuples;
        }

        private double[,] ConvertToMatrix(List<Tuple<int, int, double>> tuples, List<int> ids)
        {
            double[,] matrix = new double[tuples.Count / 2, tuples.Count / 2];
            var indicies = GetArrayIndicies(ids);

            foreach (var tuple in tuples)
            {
                matrix[indicies[tuple.Item1], indicies[tuple.Item2]] = tuple.Item3;
            }

            return matrix;
        }

        private Dictionary<int, int> GetArrayIndicies(List<int> ids)
        {
            var dict = new Dictionary<int, int>();
            for (var i = 0; i < ids.Count; i++)
            {
                dict.Add(ids[i], i);
            }

            return dict;
        }

        private bool HasWinner(double[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                var answerIsWinner = true;
                for (int j = 0; j < matrix.GetLength(0); j++)
                {
                    if (matrix[i, j] < matrix[j, i])
                    {
                        answerIsWinner = false;
                        break;
                    }
                }
                if (answerIsWinner)
                {
                    return true;
                }
            }

            return false;
        }

        private int? HasWinner(List<CondocertResult> results)
        {
            // results.Select(r => r.Answer1Id).Distinct().Where(a1 => result);

            var winner = results
                .GroupBy(r => r.Answer1Id)
                .FirstOrDefault(g => g.All(h =>
                        h.Margin > results.First(r => r.Answer2Id == h.Answer1Id && r.Answer2Id == h.Answer1Id)?.Margin
                    )
                )?.First();


            return winner?.Answer1Id;
        }

        private List<CondocertResult> ResolveAmbiguity(List<CondocertResult> results)
        {
            var min = results.Where(r => r.Margin > 0).Aggregate((curMin, r) => r.Margin.CompareTo(curMin?.Margin) > 0 ? r : curMin);
            results.Remove(min);
            return results;
        }
    }
}