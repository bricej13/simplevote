﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Enums;
using SimpleVote.Models;
using SimpleVote.Objects;

namespace SimpleVote.Services
{
    public class ResultIrService : IResultService
    {
        private readonly DatabaseContext _db = new DatabaseContext();
        private readonly IMapper _mapper;

        public ResultIrService(IMapper mapper)
        {
            _mapper = mapper;
        }

        public IList<IResultDto> GetResults(string uniqueId)
        {
            var results = _db.Response.Where(r => r.Ballot.UniqueId == uniqueId || r.Ballot.EditId == uniqueId)
                .Include(r => r.Answer)
                .Include(r => r.Question)
                .ToList();

            return InstantRunoffRemoveAndReduceBallot(results.Cast<ResponseRanked>().ToList());
        }


        public IList<IResultDto> InstantRunoffRemoveAndReduceBallot(ICollection<ResponseRanked> responses)
        {
            var result = new List<ResultIrDto>();
            IList<ResponseRanked> reassignedResponses = new List<ResponseRanked>();

            // TODO: return if there is a plurality

            responses
                .GroupBy(r => r.Question)
                .Select(g => new {Question = g.Key, Answers = g.ToList()})
                .ToList()
                .ForEach(g =>
                {
                    var removedAnswers = new List<int>();

                    // Loop over iterations
                    for (int i = 0; i < g.Answers.Count - 1; i++)
                    {
                        // Locate losing answer
                        var losingAnswerId = GetLosingAnswer(responses, removedAnswers);

                        // Reassign votes
                        reassignedResponses = ReAssignRanks(responses.ToList(), losingAnswerId);
                    }
                    result.Add(new ResultIrDto
                    {
                        Answer =
                            _mapper.Map<Answer, AnswerDto>(reassignedResponses.First(r => r.Rank.Equals(1)).Answer),
                        Question = _mapper.Map<Question, QuestionDto>(reassignedResponses.First(r => r.Rank.Equals(1))
                            .Question),
                        Rounds = reassignedResponses
                            .GroupBy(r => new {r.Answer, r.Rank})
                            .Select(group => new ResultIrAnswerDto()
                            {
                                Answer = _mapper.Map<Answer, AnswerDto>(group.Key.Answer),
                                Count = group.Count(),
                                Rank = group.Key.Rank
                            }).ToList()
                    });
                });


            return result.Cast<IResultDto>().ToList();
        }

        public IList<IrAnswerCount> GetAnswerCounts(IEnumerable<Response> responses)
        {
            return responses.Cast<ResponseRanked>()
                .GroupBy(r => new {AnswerId = r.Answer.Id, QuestionId = r.Question.Id})
                .Select(g => new IrAnswerCount()
                {
                    QuestionId = g.Key.QuestionId,
                    AnswerId = g.Key.AnswerId,
                    OneCount = g.Count(r => r.Rank.Equals(1)),
                    TwoCount = g.Count(r => r.Rank.Equals(2)),
                    ThreeCount = g.Count(r => r.Rank.Equals(3))
                })
                .OrderByDescending(a => a.OneCount)
                .ThenByDescending(a => a.TwoCount)
                .ThenByDescending(a => a.ThreeCount)
                .ToList();
        }

        public int GetLosingAnswer(IEnumerable<Response> responses, List<int> eliminatedAnswers)
        {
            return responses.Cast<ResponseRanked>()
                .Where(r => !eliminatedAnswers.Contains(r.Answer.Id))
                .GroupBy(r => new {AnswerId = r.Answer.Id, QuestionId = r.Question.Id})
                .Select(g => new IrAnswerCount()
                {
                    QuestionId = g.Key.QuestionId,
                    AnswerId = g.Key.AnswerId,
                    OneCount = g.Count(r => r.Rank.Equals(1)),
                    TwoCount = g.Count(r => r.Rank.Equals(2)),
                    ThreeCount = g.Count(r => r.Rank.Equals(3))
                })
                .OrderByDescending(a => a.OneCount)
                .ThenByDescending(a => a.TwoCount)
                .ThenByDescending(a => a.ThreeCount)
                .ToList()
                .GroupBy(c => c.QuestionId)
                .Select(g => g.Last().AnswerId)
                .FirstOrDefault();
        }

        public IList<ResponseRanked> ReAssignRanks(ICollection<ResponseRanked> responses, int losingAnswerId)
        {
            // Find users on the losing end
            var losingUsers = responses
                .Where(r => r.Answer.Id.Equals(losingAnswerId) && r.Rank.Equals(1))
                .Select(r => r.SubmittedBy)
                .ToList();

            // Reduce rank of all answers by losing users
            responses.Where(r => losingUsers.Contains(r.SubmittedBy)).ToList().ForEach(r => r.Rank--);

            return responses.ToList().SkipWhile(r => r.Answer.Id.Equals(losingAnswerId)).ToList();
        }
    }
}