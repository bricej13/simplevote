﻿using System;
using AutoMapper;
using SimpleVote.Enums;
using SimpleVote.Models;

namespace SimpleVote.Services
{
    public class ResultServiceFactory : IResultServiceFactory
    {
        private readonly IBallotService _ballotService;
        private readonly IResultService _fptpResultService;
        private readonly IResultService _irResultService;
        private readonly IResultService _approvalResultService;
        private readonly IResultService _condocertResultService;

        public ResultServiceFactory(IMapper mapper, IBallotService ballotService)
        {
            _ballotService = ballotService;
            _fptpResultService = new ResultFptpService(mapper);
            _irResultService = new ResultIrService(mapper);
            _approvalResultService = new ResultApprovalService(mapper);
            _condocertResultService = new ResultCondocertService(mapper);
        }

        public IResultService GetService(string uniqueId)
        {
            var ballot = _ballotService.GetByUniqueId(uniqueId);

            switch (ballot.VoteType)
            {
                case VoteTypeEnum.FirstPastThePost:
                    return _fptpResultService;
                case VoteTypeEnum.InstantRunoff:
                    return _irResultService;
                case VoteTypeEnum.Approval:
                    return _approvalResultService;
                case VoteTypeEnum.Condocert:
                    return _condocertResultService;
                default:
                    throw new ArgumentOutOfRangeException(nameof(ballot.VoteType), ballot.VoteType, null);
            }
        }
    }
}