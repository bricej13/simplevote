﻿using System.Linq;
using AutoMapper;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;

namespace SimpleVote.Services
{
    public class AnswerService : IAnswerService
    {
        private readonly DatabaseContext _db = new DatabaseContext();
        private readonly IMapper _mapper;

        public AnswerService(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Answer Create(AnswerCreateDto answerDto)
        {
            var question = _db.Question.Find(answerDto.QuestionId);
            question.Answers.Add(_mapper.Map<AnswerCreateDto, Answer>(answerDto));
            _db.SaveChanges();
            return question.Answers.FirstOrDefault();
        }

        public void Delete(int id)
        {
            var answer = _db.Answer.Find(id);
            _db.Answer.Remove(answer);
            _db.SaveChanges();
        }
    }
}