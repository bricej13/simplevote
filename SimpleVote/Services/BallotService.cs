﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;

namespace SimpleVote.Services
{
    public class BallotService : IBallotService
    {
        private static readonly Random Rnd = new Random();
        private readonly DatabaseContext _db = new DatabaseContext();
        private readonly IMapper _mapper;

        public BallotService(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Ballot Get(int id)
        {
            var entity = _db.Ballots.Include(b => b.Questions).ThenInclude(q => q.Answers)
                .FirstOrDefault(p => p.Id.Equals(id));
            return entity;
        }

        public Ballot GetByUniqueId(string uniqueId)
        {
            var entity = _db.Ballots.Include(b => b.Questions).ThenInclude(q => q.Answers)
                .FirstOrDefault(p => p.UniqueId.Equals(uniqueId) || p.EditId.Equals(uniqueId));
            return entity;
        }

        public void Delete(int id)
        {
            var ballot = Get(id);

            // Remove Answers
            ballot.Questions.SelectMany(q => q.Answers).ToList().ForEach(a => _db.Answer.Remove(a));

            // Remove Questions
            ballot.Questions.ToList().ForEach(q => _db.Question.Remove(q));

            // Remove Ballot
            _db.Ballots.Where(b => b.Id == id).ToList().ForEach(b => _db.Ballots.Remove(b));

            _db.SaveChanges();
        }

        public Ballot CreateNewBallot(BallotCreateDto ballotDto)
        {
            var ballot = _mapper.Map<BallotCreateDto, Ballot>(ballotDto);
            ballot.UniqueId = GenerateUniqueId();
            ballot.EditId = GenerateUniqueId();

            if (ballot.SingleQuestion)
            {
                var question = new Question()
                {
                    Description = ballotDto.Name
                };
                ballot.Questions = new List<Question>() {question};
            }

            var result = _db.Ballots.Add(ballot);
            _db.SaveChanges();
            return result.Entity;
        }

        public Ballot UpdateBallot(BallotDto ballotDto)
        {
            var existing = _db.Ballots.Find(ballotDto.Id);
            existing.CloseDate = ballotDto.CloseDate;
            existing.OpenDate = ballotDto.OpenDate;
            existing.IsClosed = ballotDto.IsClosed;
            existing.IsOpen = ballotDto.IsOpen;
            existing.Name = ballotDto.Name;
            existing.SingleQuestion = ballotDto.SingleQuestion;
            
            var result = _db.Ballots.Update(existing);
            _db.SaveChanges();
            return result.Entity;
        }

        private string GenerateUniqueId()
        {
            return Guid.NewGuid().ToString("n") +
                   ShuffleString(Guid.NewGuid().ToString("n"));
        }


        static void FisherYates(int[] array)
        {
            var arraysize = array.Length;
            int random;
            int temp;

            for (int i = 0; i < arraysize; i++)
            {
                random = i + (int) (Rnd.NextDouble() * (arraysize - i));

                temp = array[random];
                array[random] = array[i];
                array[i] = temp;
            }
        }

        private static string ShuffleString(string s)
        {
            var output = "";
            var arraysize = s.Length;
            var randomArray = new int[arraysize];

            for (int i = 0; i < arraysize; i++)
            {
                randomArray[i] = i;
            }

            FisherYates(randomArray);

            for (var i = 0; i < arraysize; i++)
            {
                output += s[randomArray[i]];
            }

            return output;
        }
    }
}