﻿using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;

namespace SimpleVote.Services
{
    public interface IBallotService
    {
        Ballot Get(int id);
        Ballot GetByUniqueId(string uniqueId);
        void Delete(int id);
        Ballot CreateNewBallot(BallotCreateDto ballotDto);
        Ballot UpdateBallot(BallotDto ballotDto);
    }
}