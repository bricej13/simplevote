﻿using System;
using AutoMapper;
using SimpleVote.Enums;

namespace SimpleVote.Services
{
    public interface IResultServiceFactory
    {
        IResultService GetService(string uniqueId);
    }
}