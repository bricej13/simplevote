﻿using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;

namespace SimpleVote.Services
{
    public interface IQuestionService
    {
        Question Create(QuestionCreateDto dto);
        void Delete(int id);
    }
}