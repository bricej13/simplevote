﻿using System.Collections.Generic;
using SimpleVote.Controllers.Dtos;

namespace SimpleVote.Services
{
    public interface IResultService
    {
        IList<IResultDto> GetResults(string uniqueId);
    }
}