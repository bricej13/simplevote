﻿using System.Collections.ObjectModel;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;

namespace SimpleVote.Services
{
    public interface IResponseService
    {
        int Save(Collection<ResponseDto> dto);
    }
}