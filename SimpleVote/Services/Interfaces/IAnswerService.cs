﻿using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;

namespace SimpleVote.Services
{
    public interface IAnswerService
    {
        Answer Create(AnswerCreateDto answerDto);
        void Delete(int id);
    }
}