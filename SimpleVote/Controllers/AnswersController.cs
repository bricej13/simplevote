﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;
using SimpleVote.Services;

namespace SimpleVote.Controllers
{
    [Route("api/[controller]")]
    public class AnswersController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IAnswerService _answerService;

        public AnswersController(IMapper mapper, IAnswerService answerService)
        {
            _mapper = mapper;
            _answerService = answerService;
        }

        [HttpPost]
        public IActionResult CreateAnswer([FromBody] AnswerCreateDto answerDto)
        {
            var result = _answerService.Create(answerDto);
            return Ok(_mapper.Map<Answer, AnswerDto>(result));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteAnswer(int id)
        {
            try
            {
                _answerService.Delete(id);
                return Ok();
            }
            catch (Exception e)
            {
                return NotFound();
            }
        }
    }
}