﻿using System;
using System.Collections.ObjectModel;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Exceptions;
using SimpleVote.Services;

namespace SimpleVote.Controllers
{
    [Route("api/[controller]")]
    public class ResponsesController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IResponseService _responseService;

        public ResponsesController(IMapper mapper, IResponseService responseService)
        {
            _mapper = mapper;
            _responseService = responseService;
        }

        [HttpPost()]
        public IActionResult SubmitResponse([FromBody] Collection<ResponseDto> responseDto)
        {
            try
            {
                return Ok(_responseService.Save(responseDto));
            }
            catch (DuplicateUserResponseException e)
            {
                return BadRequest("Duplicate response from this computer. That seems very suspicious.");
            }
            catch (BallotNotOpenException e)
            {
                return BadRequest("Voting is currently closed.");
            }
            catch (EmptyBallotResponseException e)
            {
                return BadRequest("Ballot response was empty");
            }
        }
    }
}