﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;
using SimpleVote.Services;

namespace SimpleVote.Controllers
{
    [Route("api/[controller]")]
    public class BallotsController : Controller
    {
        private readonly DatabaseContext _db = new DatabaseContext();
        private readonly IMapper _mapper;
        private readonly IBallotService _ballotService;
        private readonly IResultServiceFactory _resultServiceFactory;

        public BallotsController(IMapper mapper, IBallotService ballotService, IResultServiceFactory resultServiceFactory)
        {
            _mapper = mapper;
            _ballotService = ballotService;
            _resultServiceFactory = resultServiceFactory;
        }

        [HttpGet]
        public IEnumerable<Ballot> Get()
        {
            return _db.Ballots.ToList();
        }

        [HttpGet("{uniqueId}")]
        public IActionResult Get(string uniqueId)
        {
            var ballot = _mapper.Map<Ballot, BallotDto>(_ballotService.GetByUniqueId(uniqueId));
            if (ballot != null)
            {
                return Ok(ballot);
            }

            return NotFound();
        }

        [HttpPost]
        public IActionResult CreateNewBallot([FromBody] BallotCreateDto ballotDto)
        {
            var result = _ballotService.CreateNewBallot(ballotDto);
            return Ok(_mapper.Map<Ballot, BallotDto>(result));
        }

        [HttpPut]
        public IActionResult UpdateBallot([FromBody] BallotDto ballotDto)
        {
            var result = _ballotService.UpdateBallot(ballotDto);
            return Ok(_mapper.Map<Ballot, BallotDto>(result));
        }

        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                _ballotService.Delete(id);
                return Ok();
            }
            catch (DbUpdateException e)
            {
                return NotFound(e.Message);
            }
        }

        [HttpGet("{uniqueId}/results")]
        public IActionResult GetResults(string uniqueId)
        {
            return Ok(_resultServiceFactory.GetService(uniqueId).GetResults(uniqueId));
        }
    }
}