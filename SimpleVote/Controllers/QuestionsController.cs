﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;
using SimpleVote.Services;

namespace SimpleVote.Controllers
{
    [Route("api/[controller]")]
    public class QuestionsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly IQuestionService _questionService;

        public QuestionsController(IMapper mapper, IQuestionService questionService)
        {
            _mapper = mapper;
            _questionService = questionService;
        }

        [HttpPost]
        public IActionResult CreateQuestion([FromBody] QuestionCreateDto questionDto)
        {
            var result = _questionService.Create(questionDto);
            return Ok(_mapper.Map<Question, QuestionDto>(result));
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteQuestion(int id)
        {
            try
            {
                _questionService.Delete(id);
                return Ok();
            }
            catch (DbUpdateException e)
            {
                return NotFound();
            }
        }
    }
}