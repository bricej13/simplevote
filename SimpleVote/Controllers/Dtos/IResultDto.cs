﻿namespace SimpleVote.Controllers.Dtos
{
    public interface IResultDto
    {
        QuestionDto Question { get; set; }
    }
}