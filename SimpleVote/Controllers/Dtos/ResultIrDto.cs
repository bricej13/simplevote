﻿using System.Collections.Generic;

namespace SimpleVote.Controllers.Dtos
{
    public class ResultIrDto : IResultDto
    {
        public QuestionDto Question { get; set; }
        public AnswerDto Answer { get; set; }
        public List<ResultIrAnswerDto> Rounds { get; set; }
    }
}