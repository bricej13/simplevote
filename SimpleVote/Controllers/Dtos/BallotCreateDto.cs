﻿using System;
using SimpleVote.Enums;

namespace SimpleVote.Controllers.Dtos
{
    public class BallotCreateDto
    {
        public string Name { get; set; }
        public VoteTypeEnum? VoteType { get; set; }
        public bool SingleQuestion { get; set; }
    }
}