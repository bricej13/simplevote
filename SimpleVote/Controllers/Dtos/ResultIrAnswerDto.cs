﻿namespace SimpleVote.Controllers.Dtos
{
    public class ResultIrAnswerDto : IResultDto
    {
        public int Count { get; set; }
        public int Rank { get; set; }
        public AnswerDto Answer { get; set; }
        public QuestionDto Question { get; set; }
    }
}