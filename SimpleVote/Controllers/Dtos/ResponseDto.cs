﻿using Newtonsoft.Json;
using SimpleVote.Enums;
using SimpleVote.Mappers;

namespace SimpleVote.Controllers.Dtos
{
    [JsonConverter(typeof(ResponseConverter))]
    public class ResponseDto
    {
        public string SessionId { get; set; }
        public int BallotId { get; set; }
        public int QuestionId { get; set; }
        public VoteTypeEnum VoteType { get; set; }
    }
}