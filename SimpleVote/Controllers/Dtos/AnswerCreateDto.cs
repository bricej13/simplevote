﻿namespace SimpleVote.Controllers.Dtos
{
    public class AnswerCreateDto
    {
        public string Description { get; set; }
        public int QuestionId { get; set; }
    }
}