﻿namespace SimpleVote.Controllers.Dtos
{
    public class QuestionCreateDto
    {
        public string Description { get; set; }
        public int BallotId { get; set; }
    }
}