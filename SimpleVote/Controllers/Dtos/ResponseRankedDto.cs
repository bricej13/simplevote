﻿using System;
using System.Collections.Generic;

namespace SimpleVote.Controllers.Dtos
{
    public class ResponseRankedDto : ResponseDto
    {
        /// <summary>
        /// Dictionary of AnswerId:Rank for responses.
        /// The key is the AnswerId, and the value is the Rank
        /// </summary>
        public Dictionary<int, int> Ranks { get; set; }
    }
}