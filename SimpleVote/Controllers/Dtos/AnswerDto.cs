﻿using System;
using System.Collections.Generic;
using Microsoft.CodeAnalysis.CSharp.Syntax;

namespace SimpleVote.Controllers.Dtos
{
    public class AnswerDto : IComparable
    {
        public Int32? Id { get; set; }
        public string Description { get; set; }

        private  class AnswerDtoEqualityComparer : IEqualityComparer<AnswerDto>
        {
            public bool Equals(AnswerDto x, AnswerDto y)
            {
                if (ReferenceEquals(x, y)) return true;
                if (ReferenceEquals(x, null)) return false;
                if (ReferenceEquals(y, null)) return false;
                if (x.GetType() != y.GetType()) return false;
                return x.Id == y.Id;
            }

            public int GetHashCode(AnswerDto obj)
            {
                return obj.Id.GetHashCode();
            }
        }

        public int CompareTo(object obj)
        {
            if (obj.GetType() == this.GetType())
            {
                if (((AnswerDto) obj).Id.HasValue && Id.HasValue)
                {
                    return Id.Value.CompareTo(((AnswerDto) obj).Id.Value);
                }
            }

            return -1;
        }
    }
}