﻿
using System.Collections.Generic;

namespace SimpleVote.Controllers.Dtos
{
    public class ResponseApprovalDto : ResponseDto
    {
        public List<int> Answers { get; set; }
    }
}