﻿namespace SimpleVote.Controllers.Dtos
{
    public class ResultApprovalDto : IResultDto
    {
        public int Count { get; set; }
        public AnswerDto Answer { get; set; }
        public QuestionDto Question { get; set; }
    }
}