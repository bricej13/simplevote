﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using SimpleVote.Enums;

namespace SimpleVote.Controllers.Dtos
{
    public class BallotDto
    {
        public int? Id { get; set; }
        public string UniqueId { get; set; }
        public string EditId { get; set; }
        public string Name { get; set; }
        public bool SingleQuestion { get; set; }
        public bool IsOpen { get; set; }
        public bool IsClosed { get; set; }
        public VoteTypeEnum? VoteType { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        
        public ICollection<QuestionDto> Questions { get; set; }

        public BallotDto()
        {
            this.Questions = new List<QuestionDto>();
        }
    }
}
