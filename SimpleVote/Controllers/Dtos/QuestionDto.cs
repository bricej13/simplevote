﻿using System.Collections.Generic;

namespace SimpleVote.Controllers.Dtos
{
    public class QuestionDto
    {
        public int? Id { get; set; }
        public string Description { get; set; }
        public IEnumerable<AnswerDto> Answers { get; set; }

        public QuestionDto()
        {
            Answers = new List<AnswerDto>();
        }
    }
}