﻿using System.Collections.Generic;
using SimpleVote.Models;

namespace SimpleVote.Controllers.Dtos
{
    public class ResultCondocertDto : IResultDto
    {
        public QuestionDto Question { get; set; }
        public List<AnswerDto> Answers { get; set; }
        public List<CondocertResult> Margins { get; set; }
        public Dictionary<AnswerDto, List<CondocertResult>> Matrix { get; set; }
    }
}