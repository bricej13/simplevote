﻿using Microsoft.AspNetCore.Mvc;

namespace SimpleVote.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var path = "wwwroot/index.html";

            var fileBytes = System.IO.File.ReadAllBytes(path);            

            FileContentResult file = File(fileBytes, "text/html");

            return file;
        }
    }
}