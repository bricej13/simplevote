﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SimpleVote.Controllers.Dtos;
using SimpleVote.Models;
using SimpleVote.Services;

namespace SimpleVote.Controllers
{
    [Route("api/[controller]")]
    public class VoteTypeController : Controller
    {
        private readonly DatabaseContext _db = new DatabaseContext();

        public VoteTypeController()
        {
        }

        [HttpGet]
        [ResponseCache(Duration = 6000)]
        public IActionResult GetVoteTypes()
        {
            return Ok(_db.VoteType.ToList());
        }
    }
}